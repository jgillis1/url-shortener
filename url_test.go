package main

import (
	"crypto/sha256"
	"fmt"
	"io"
	"strings"
	"testing"
)

func TestUrlStruct(t *testing.T) {
	url, err := NewUrl("http://www.google.com")
	if err != nil {
		t.Errorf("Error Creating URL: %s", err.Error())
	}

	if url.Path != "http://www.google.com" {
		t.Errorf("Got %s, want: www.google.com", url.Path)
	}
}

func TestShortenUrl(t *testing.T) {
	urlString := "https://www.google.com"
	url, err := NewUrl(urlString)
	if err != nil {
		t.Errorf("Error Creating URL: %s", err.Error())
	}

	input := strings.NewReader(urlString)
	hash := sha256.New()
	if _, err := io.Copy(hash, input); err != nil {
		t.Errorf("Error creating hash: %v", err)
	}
	sum := hash.Sum(nil)
	want := fmt.Sprintf("%x", sum)[:7]

	if want != url.Token {
		t.Errorf("Want %s, but got %s", want, url.Token)
	}

}
