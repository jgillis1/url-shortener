package main

import (
	"crypto/sha256"
	"fmt"
	"io"
	"strings"
)

type Url struct {
	Path  string
	Token string
}

func NewUrl(path string) (url Url, err error) {
	u := Url{Path: path}
	if err = u.shortenUrl(); err != nil {
		return Url{}, fmt.Errorf("Error creating url: %s", err.Error())
	}

	return u, nil

}

func (u *Url) shortenUrl() (e error) {
	input := strings.NewReader(u.Path)
	hash := sha256.New()
	if _, err := io.Copy(hash, input); err != nil {
		return err
	}
	sum := hash.Sum(nil)
	u.Token = fmt.Sprintf("%x", sum)[:7]
	return nil
}
